#!/bin/csh -f

alias MATH 'set \!:1 = `echo "\!:3-$" | bc -l`'
#**********************************************************************************************************
#  genraypath
#  A routine to generate raypaths using Taup routine. 
#  Created by Carlos Alberto Chaves on 07/01/2014.
#  University of Sao Paulo - University of Michigan
#  carlos.chaves@iag.usp.br; cchaves@umich.edu; calbertochaves@gmail.com (main)
#  version 1.0
# **********************************************************************************************************


gfortran -c prem.f90
gfortran -c stw105.f90
gfortran -c reads362ani.f90
gfortran -O3 -Wall -Wtabs umich_cj_ttc.f90 prem.o stw105.o reads362ani.o -o xumich_cj_ttc

#set fileinput = STATIONS
#set evdepth = 555
#set evla = -20.7200
#set evlo = -178.1600
#set it = 1      
#foreach line ( "`cat ${fileinput}`" )
#	set argv = ( $line )
#	set stla = $3
#	set stlo = $4
#	if ( $it > 1 ) then
#		echo ${evdepth} ${evla} ${evlo} ${stla} ${stlo} >> eventinfo.txt
#	else
#		echo ${evdepth} ${evla} ${evlo} ${stla} ${stlo} > eventinfo.txt
#	endif
#	@ it++	
#end

set intmethod = simpson
#set intmethod = trapezoidal
set onedmodel = PREM
set threedmodel = s20_l12
#set threedmodel = p12_linear_1.00_6.67
#set threedmodel = p12_2lines_0.33_0.12
set phase = S
set kfile = sph
set crust = 'false'

echo $intmethod > input
echo $onedmodel >> input
echo $threedmodel >> input
echo $crust >> input
echo $phase >> input
echo $kfile >> input 

set phases = 'S,Sdiff'
set model = prem
set file_evt_sta = P_and_S_info_specfem.txt #This file contains information regarding events and stations


#set it = 1
#foreach line ( "`cat ${file_evt_sta}`" )
#	set argv = ( $line )
#	set cmtcode = $1
#	set staname = $2
#	set evtla = $3
#	set evtlo = $4
#	set depth = $5
#	set stala = $6
#	set stalo = $7
#	set staelv = $8
#	echo $it $cmtcode $staname $evtla $evtlo $depth $stala $stalo $staelv > evt
#	
#	taup_path -mod ${model} -h ${depth} -evt ${evtla} ${evtlo} -sta ${stala} ${stalo} -ph ${phases} -o trash1

#	sed '1d' trash1.gmt > trash6.txt 
#	sed -n '1,/>/p' trash6.txt | sed '/>/,$d' > raypath
#	cat raypath | wc -l > noflines
#	rm trash*
#	if ( $it > 1 ) then
#		./xumich_cj_ttc >> ttc_file_${threedmodel}_${phases}
#	else
#		./xumich_cj_ttc > ttc_file_${threedmodel}_${phases}
#	endif	
#	@ it++
#end
#echo "Done!"


set linenum = 1
set flinenum = $linenum
set nlines = `wc -l ${file_evt_sta} | awk '{print $1}'`
echo 'Number of records: ' $nlines
echo 'Starting the calculation of travel time delays...'
while ( $linenum <= $nlines )


        sed -n ${linenum}p ${file_evt_sta} > file_evt_sta_${linenum}  
	set cmtcode = `awk '{print $1}' file_evt_sta_${linenum}`	
	set staname = `awk '{print $2}' file_evt_sta_${linenum}`
	set evtla = `awk '{print $3}' file_evt_sta_${linenum}` 
	set evtlo = `awk '{print $4}' file_evt_sta_${linenum}`
	set depth = `awk '{print $5}' file_evt_sta_${linenum}`
	set stala = `awk '{print $6}' file_evt_sta_${linenum}`
	set stalo = `awk '{print $7}' file_evt_sta_${linenum}`
	set staelv = `awk '{print $8}' file_evt_sta_${linenum}`

	echo $linenum $cmtcode $staname $evtla $evtlo $depth $stala $stalo $staelv > evt
	
	taup_path -mod ${model} -h ${depth} -evt ${evtla} ${evtlo} -sta ${stala} ${stalo} -ph ${phases} -o trash1

	sed '1d' trash1.gmt > trash6.txt 
	sed -n '1,/>/p' trash6.txt | sed '/>/,$d' > raypath
	cat raypath | wc -l > noflines
	rm trash*
	if ( $linenum > 1 ) then
		./xumich_cj_ttc >> ttc_file_${threedmodel}_${phases}_specfem
	else
		./xumich_cj_ttc > ttc_file_${threedmodel}_${phases}_specfem
	endif
	rm file_evt_sta_${linenum} 		
        @ linenum ++
	
end
echo "Done!"  

