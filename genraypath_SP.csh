#!/bin/csh -f

alias MATH 'set \!:1 = `echo "\!:3-$" | bc -l`'
#**********************************************************************************************************
#  genraypath
#  A routine to generate raypaths using Taup routine. 
#  Created by Carlos Alberto Chaves on 07/01/2014.
#  University of Sao Paulo - University of Michigan
#  carlos.chaves@iag.usp.br; cchaves@umich.edu; calbertochaves@gmail.com (main)
#  version 1.0
# **********************************************************************************************************


#input file: cmtcode, station name, event latitude, event longitude, event depth (km), station latitude, station longitude, station elevation (km)   


gfortran -c prem.f90
gfortran -c stw105.f90
gfortran -c reads362ani.f90
gfortran -O3 -Wall -Wtabs umich_cj_ttc.f90 prem.o stw105.o reads362ani.o -o xumich_cj_ttc


set intmethod = simpson
set onedmodel = PREM
set threedmodel_S = s20_l12
set threedmodel_P = P12
set wave_1 = S
set wave_2 = P
set kfile = sph
set crust = 'false'

set phase_1 = 'S'
set phase_2 = 'P'
set phase = 'SP'
set model = prem

set file_evt_sta = P_and_S_info_specfem.txt 

set linenum = 1
set flinenum = $linenum
set nlines = `wc -l ${file_evt_sta} | awk '{print $1}'`
echo 'Number of records: ' $nlines
echo 'Starting the calculation of travel time delays...'
while ( $linenum <= $nlines )
	echo $linenum 

        sed -n ${linenum}p ${file_evt_sta} > file_evt_sta_${linenum}  
	set cmtcode = `awk '{print $1}' file_evt_sta_${linenum}`	
	set staname = `awk '{print $2}' file_evt_sta_${linenum}`
	set evtla = `awk '{print $3}' file_evt_sta_${linenum}` 
	set evtlo = `awk '{print $4}' file_evt_sta_${linenum}`
	set depth = `awk '{print $5}' file_evt_sta_${linenum}`
	set stala = `awk '{print $6}' file_evt_sta_${linenum}`
	set stalo = `awk '{print $7}' file_evt_sta_${linenum}`
	set staelv = `awk '{print $8}' file_evt_sta_${linenum}`

	echo $linenum $cmtcode $staname $evtla $evtlo $depth $stala $stalo $staelv > evt


        taup_pierce -mod ${model} -h ${depth} -evt ${evtla} ${evtlo} -sta ${stala} ${stalo} -ph ${phase} -under -o trash1_pierce

	set lat_p = `awk 'NR==3 {print $4}' trash1_pierce.gmt`
	set lon_p = `awk 'NR==3 {print $5}' trash1_pierce.gmt`


	echo $intmethod > input
	echo $onedmodel >> input
	echo $threedmodel_S >> input
	echo $crust >> input
	echo $wave_1 >> input
	echo $kfile >> input 


	taup_path -mod ${model} -h ${depth} -evt ${evtla} ${evtlo} -sta ${lat_p} ${lon_p} -ph ${phase_1} -o trash1

	sed '1d' trash1.gmt > trash6.txt 
	sed -n '1,/>/p' trash6.txt | sed '/>/,$d' > raypath
	cat raypath | wc -l > noflines
	rm trash*
	if ( $linenum > 1 ) then
		./xumich_cj_ttc >> ttc_file_${threedmodel_S}_${phase_1}
	else
		./xumich_cj_ttc > ttc_file_${threedmodel_S}_${phase_1}
	endif

	echo $intmethod > input
	echo $onedmodel >> input
	echo $threedmodel_P >> input
	echo $crust >> input
	echo $wave_2 >> input
	echo $kfile >> input 


	taup_path -mod ${model} -h 0 -evt ${lat_p} ${lon_p} -sta ${stala} ${stalo}  -ph ${phase_2}  -o trash1

	sed '1d' trash1.gmt > trash6.txt 
	sed -n '1,/>/p' trash6.txt | sed '/>/,$d' > raypath
	cat raypath | wc -l > noflines
	rm trash*
	if ( $linenum > 1 ) then
		./xumich_cj_ttc >> ttc_file_${threedmodel_P}_${phase_2}
	else
		./xumich_cj_ttc > ttc_file_${threedmodel_P}_${phase_2}
	endif
	rm file_evt_sta_${linenum} 

		
        @ linenum ++
	
end
echo "Done!"  

